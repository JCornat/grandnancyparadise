'use strict';

(function () {
    var app = angular.module('app', [
        'ngCordova', 'ui.router', 'ngAnimate', 'ngSanitize', 'ngTouch'
    ]);

    app.config(['$stateProvider', '$urlRouterProvider', '$compileProvider', function ($stateProvider, $urlRouterProvider, $compileProvider) {
        $stateProvider
            .state('home', {
                url: '/',
                views: {
                    '': {
                        templateUrl: 'view/home.html',
                        controller: 'HomeCtrl as ctrl'
                    }
                }
            })
            .state('map', {
                url: '/map',
                views: {
                    '': {
                        templateUrl: 'view/map.html',
                        controller: 'MapCtrl as ctrl'
                    }
                }
            });
        $urlRouterProvider.otherwise('/');
    }]);

    app.run(['$rootScope', 'globalService', function($rootScope, globalService) {

        document.addEventListener('deviceready', onDeviceReady, false);

        function onDeviceReady() {
            $rootScope.previousState = {};
            $rootScope.currentState = {};
            $rootScope.$on('$stateChangeSuccess', function(ev, to, toParams, from, fromParams) {
                $rootScope.previousState = {name: from.name, params: fromParams};
                $rootScope.currentState = {name: to.name, params: toParams};
            });
        }
    }]);

    app.controller('HomeCtrl', ['$scope', '$state', 'globalService', '$http', function ($scope, $state, globalService, $http) {
        var self = this;

        document.addEventListener('deviceready', onDeviceReady, false);

        function onDeviceReady() {
            document.addEventListener('backbutton', backbuttonListener);

            function backbuttonListener(e) {
                e.preventDefault();
                e.stopPropagation();
            }

            $http.get('asset/data/donnees.json')
                .success(function (data) {
                    console.log(data);
                    self.data = data;
                });

            self.go = function (name) {
                globalService.city = name;
                $state.go('map');
            };

            $scope.$on('$destroy', function() {
                document.removeEventListener('backbutton', backbuttonListener);
            });
        }
    }]);

    app.factory('globalService', [function () {
        var city = '';
        return {
            city: city
        }
    }]);

    app.controller('MapCtrl', ['$scope', '$state', 'globalService', '$cordovaDeviceOrientation', '$interval', function ($scope, $state, globalService, $cordovaDeviceOrientation, $interval) {
        var self = this;

        document.addEventListener('deviceready', onDeviceReady, false);

        function onDeviceReady() {
            document.addEventListener('backbutton', backbuttonListener);

            function backbuttonListener(e) {
                e.preventDefault();
                e.stopPropagation();
                self.back();
            }

            self.city = globalService.city;
            self.displayModal = false;
            self.displayModalAnswer = false;
            self.displayModalEnd = false;
            self.currentPosition = null;
            self.displayModalCompass = true;

            self.back = function () {
                $state.go('home');
            };

            self.initMap = function (lat, lng) {
                self.map = new google.maps.Map(document.getElementById('map'), {
                    scrollwheel: false,
                    disableDefaultUI: true,
                    zoom: 17
                });
                self.map.set("styles",[ {
                    "featureType": "poi",
                    "elementType": "all",
                    "stylers": [
                        {
                            "visibility": "off"
                        }
                    ]
                },
                    {
                        "featureType": "poi.attraction",
                        "elementType": "all",
                        "stylers": [
                            {
                                "visibility": "off"
                            }
                        ]
                    },
                    {
                        "featureType": "poi.attraction",
                        "elementType": "geometry",
                        "stylers": [
                            {
                                "visibility": "off"
                            }
                        ]
                    },
                    {
                        "featureType": "poi.park",
                        "elementType": "all",
                        "stylers": [
                            {
                                "visibility": "on"
                            }
                        ]
                    },
                    {
                        "featureType": "poi.place_of_worship",
                        "elementType": "all",
                        "stylers": [
                            {
                                "visibility": "off"
                            }
                        ]
                    },
                    {
                        "featureType": "transit",
                        "elementType": "all",
                        "stylers": [
                            {
                                "visibility": "off"
                            }
                        ]
                    }
                ]);


                self.currentPosition = new google.maps.Marker({
                    map: self.map,
                    position: self.currentQuestion.currentPosition,
                    icon: {
                        url: 'img/room.png',
                        anchor: new google.maps.Point(10, 10),
                        scaledSize: new google.maps.Size(40, 40)
                    }
                });

                self.map.setCenter(new google.maps.LatLng(lat, lng));
            };

            function getQuestions () {
                return [
                    {
                        number: 1,
                        lat: 48.693618,
                        lng: 6.183230,
                        title: 'Roi Stanislas',
                        question: 'Que pointe le roi Stanislas avec son doigt ?',
                        answers: [
                            {number: 1, text: 'Son pays natal', isAnswer: false},
                            {number: 2, text: 'Le palais du gouverneur', isAnswer: false},
                            {number: 3, text: 'Le médaillon du portrait de Louis XV', isAnswer: false},
                            {number: 4, text: 'La ligne géodésique', isAnswer: true}
                        ],
                        answer: 'Nous ne savons pas ce qu\'est la ligne géodésique... mais c\'est bien la bonne réponse',
                        currentPosition: {lat: 48.693350, lng: 6.182550}
                    },
                    {
                        number: 2,
                        lat: 48.694162,
                        lng: 6.176896,
                        title: 'Obélisque de Nancy',
                        question: 'Pour quelle occasion l\'Obélisque de Nancy a été érigé ?',
                        answers: [
                            {number: 1, text: 'En la mémoire d\'un président Français', isAnswer: true},
                            {number: 2, text: 'Pour symboliser l\'amitié Egypto/Nancéenne', isAnswer: false}
                        ],
                        answer: 'L\'amitié Egypto/Nancéenne semblait crédible tout de même...',
                        currentPosition: {lat: 48.694212, lng: 6.176889}
                    },
                    {
                        number: 3,
                        lat: 48.694072,
                        lng: 6.184346,
                        title: 'Opéra de Nancy',
                        question: 'De quel siècle l\'Opéra de Nancy date-t\'il ?',
                        answers: [
                            {number: 1, text: '17ème siècle', isAnswer: false},
                            {number: 2, text: '18ème siècle', isAnswer: false},
                            {number: 3, text: '19ème siècle', isAnswer: false},
                            {number: 4, text: '20ème siècle', isAnswer: true}
                        ],
                        answer: 'Et oui, il est tout récent !',
                        currentPosition: {lat: 48.693843, lng: 6.184303}
                    }
                ]
            }

            self.questions = getQuestions();
            self.currentNumber = 0;
            self.currentQuestion = angular.copy(self.questions[self.currentNumber]);


            self.toggleModalCompass = function (boolean) {
                self.displayModalCompass = (boolean === undefined) ? !self.displayModalCompass : boolean;
            };

            self.toggleModal = function (boolean) {
                self.displayModal = (boolean === undefined) ? !self.displayModal : boolean;
            };

            self.toggleModalAnswer = function (boolean) {
                self.displayModalAnswer = (boolean === undefined) ? !self.displayModalAnswer : boolean;
            };

            self.toggleModalEnd = function (boolean) {
                self.displayModalEnd = (boolean === undefined) ? !self.displayModalEnd : boolean;
            };

            self.validate = function () {
                for (var i = 0; i < self.currentQuestion.answers.length; i++) {
                    if (self.currentQuestion.answers[i].number === self.currentQuestion.choice) {
                        if (self.currentQuestion.answers[i].isAnswer) {
                            self.currentQuestion.correct = true;
                        } else {
                            self.currentQuestion.correct = false;
                        }
                    }
                }

                self.toggleModal(false);
                self.toggleModalAnswer(true);
            };

            self.initMap(self.currentQuestion.lat, self.currentQuestion.lng);

            self.next = function () {
                self.currentNumber++;
                if (self.currentNumber >= self.questions.length) {
                    self.toggleModalEnd(true);
                } else {
                    self.currentQuestion = angular.copy(self.questions[self.currentNumber]);
                    self.map.setCenter(new google.maps.LatLng(self.currentQuestion.lat, self.currentQuestion.lng));
                    self.currentPosition.setPosition(self.currentQuestion.currentPosition);
                    self.toggleModalAnswer(false);
                    self.toggleModalCompass(true);
                }
            };

            $interval(function () {
                $cordovaDeviceOrientation.getCurrentHeading().then(function(result) {
                    self.magneticHeading = result.magneticHeading;
                    self.trueHeading = result.trueHeading;
                    self.rotate = 'rotate('+self.magneticHeading+'deg)';
                }, function(err) {
                    // An error occurred
                });
            }, 200);

            $scope.$on('$destroy', function() {
                document.removeEventListener('backbutton', backbuttonListener);
            });
        }
    }]);
})();