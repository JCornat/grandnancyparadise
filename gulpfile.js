var gulp = require('gulp');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var sass = require('gulp-sass');
var paths = {
    styles: 'www/asset/scss/*.scss'
};

gulp.task('styles', function() {
    // Minify and copy all JavaScript (except vendor scripts)
    return gulp.src(paths.styles)
        .pipe(sass({outputStyle: 'compressed'}).on('error', sass.logError))
        .pipe(gulp.dest('www/asset/css'));
});

gulp.task('watch', function() {
    gulp.watch(paths.styles, ['styles']);
});

gulp.task('default', ['watch']);